import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VuexPersistence from 'vuex-persist'
Vue.use(Vuex)
const vuexLocal = new VuexPersistence({
    storage: window.localStorage
})
export default new Vuex.Store({
	state: {
		list: [],
		Car:[],
	},
	mutations: {
		getListAsync(state,data) {
			state.list=data
			// console.log(state.list);
		},
		getCarList(state,car){
			state.Car=car.items
			// console.log(state.Car);
		},
		// addCar(state,item){
			// console.log(item);
			
		// }
	},
	actions: {
		async getListAsync({ commit }) {
			let  {data:res}  = await axios.get('https://dahua0822-api.herokuapp.com/goods')
			commit('getListAsync',res.data)
		},
		async getCarList({ commit }) {
			let  {data:car}  = await axios.get('https://dahua0822-api.herokuapp.com/cart')
			commit('getCarList',car.data)
		}
	},
	getters:{
		zongjiuan(state){
		let trueCar=state.Car.filter(item=>item.checked)
		var num=0
			trueCar.forEach(item=>{
				num+=item.total
			})
			console.log(num);
			return num
		}
	},
	modules: {

	},
	plugins: [vuexLocal.plugin]
})
